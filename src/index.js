
import Vue from 'vue';
import './sheets/style.scss';
import card from './card.vue';
import buefy from 'buefy';
import {cardamon_grades,cardamon_table} from './data/cardamon.json';
Vue.use(buefy);
Vue.component('sp',{
    template:`
    <span class="of-blue" >
    <slot></slot>
    </span>
    `,
});

Vue.component('hero',{
    props:{
        'type':{type:String,default:""},
        'header':{required:true},
    },
    template:`
    <section :class= " 'hero ' +type">
    <div class="hero-body">
      <div class="container">
        <h1 class="title">
        {{header}}
        </h1>
        <h2 class="subtitle">
         <slot></slot>
        </h2>
      </div>
    </div>
  </section>
    `
    
})

Vue.component('icon',{
    props:{
     type:{type:String,default:''},
     ico:{type:String,default:''},
    }
    ,
    template:`
    <span :class=" 'icon '+type ">
  <i :class=" 'fa fa-'+ico"></i>
</span>
    `
});

let vue = new Vue({
    el:"#main",
  
     methods:{
     
       

     },
     mounted(){
       

        }   ,
    data:{
heading:'GMS Exports and Traders',
modal_cardamon:false,
json:'',
cardamon_table,
cardamon_grades,
},
    computed:{
        is_cardamon(){
          return (this.modal_cardamon)?'is-active':'';
        }
    }
    ,
     components:{
         card
     }
 
});